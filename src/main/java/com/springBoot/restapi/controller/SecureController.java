package com.springBoot.restapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springBoot.restapi.model.LoginDao;

/**
 * @author elianaurrutia
 *public class SecureController cotroller funcionalidad login y log del aplicativo
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/secure", consumes = "application/json", produces = "application/json")
public class SecureController {

	private LoginDao loginDao;

	public String response(String nameResponse, String cod, String descrip, String data) {
		String dato = "{\"" + nameResponse + "\":{\"INFO\":{\"Codigo\":\"" + cod + "\",\"Descripcion\":\"" + descrip
				+ "\"},\"OUTPUT\":" + data + "}}";
		return dato;

	}

	public SecureController(LoginDao loginDao) {
		super();
		this.loginDao = loginDao;
	}

	@PostMapping(path = "/Logger", consumes = "application/json", produces = "application/json")
	public String LogInsert(@RequestBody String input) {

		try {
			return loginDao.LogInsert(input);
		} catch (Exception e) {
			return response("Logger_Response", "16", "Error parametros", "{}");
		}

	}

	@PostMapping(path = "/login", consumes = "application/json", produces = "application/json")
	public String login(@RequestBody String type) {
		try {
			return loginDao.login(type);
		} catch (Exception e) {
			return response("Login_Response", "16", "Error parametros", "{}");
		}

	}

}
