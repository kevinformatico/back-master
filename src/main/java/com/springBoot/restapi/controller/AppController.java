package com.springBoot.restapi.controller;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springBoot.restapi.model.App;

/**
 * @author elianaurrutia
 *public class AppController controlador Api Funcionalidades Insert, search, update del aplicativo
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(value = "/api", consumes = "application/json", produces = "application/json")
public class AppController {

	private App appDao;

	public AppController(App appDao) {
		super();
		this.appDao = appDao;
	}

	private static final Logger log = LoggerFactory.getLogger(AppController.class);

	public String response(String nameResponse, String cod, String descrip, String data) {
		String dato = "{\"" + nameResponse + "\":{\"INFO\":{\"Codigo\":\"" + cod + "\",\"Descripcion\":\"" + descrip
				+ "\"},\"OUTPUT\":" + data + "}}";
		return dato;

	}

	@PostMapping(path = "/search", consumes = "application/json", produces = "application/json")
	public String searchByType(@RequestBody String type) {
		try {
			return appDao.searchByType(type);
		} catch (Exception e) {
			return response("SearchData_Response", "16", "Error parametros", "{}");
		}

	}

	@PostMapping(path = "/insertType", consumes = "application/json", produces = "application/json")
	public String InsertbyType(@RequestBody String type) {

		try {
			return appDao.InsertByTipe(type);
		} catch (Exception e) {
			return response("InsertData_Response", "16", "Error parametros", "{}");
		}

	}

	@PostMapping(path = "/upDateByType/{id}", consumes = "application/json", produces = "application/json")
	public String UpdatetbyType(@RequestBody String type, @PathVariable String id) {
		try {
			return appDao.UpdateByType(type, id);
		} catch (Exception e) {
			return response("UpdateData_Response", "16", "Error parametros", "{}");
		}

	}

	@PostMapping(path = "/searchVersionProductive", consumes = "application/json", produces = "application/json")
	public String searchVersionProductive(@RequestBody String data) {
		try {
			return appDao.queryVersionProductive(data);
		} catch (Exception e) {
			return response("VersionPruductive_Response", "16", "Error parametros", "{}");
		}

	}

}
