package com.springBoot.restapi.model;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author elianaurrutia
 * public class LoginDao : login y log contienen los repositorios de respuesta a ElasticsearchDB
 */
@Repository
public class LoginDao {

	private String port = "9200";

	private String INDEX = "handshakemanager";
	private String TYPE = "_doc";
	private String endpoint = "http://localhost:" + port + "/" + INDEX + "/" + TYPE;

	public String response(String nameResponse, String cod, String descrip, String data) {
		String dato = "{\"" + nameResponse + "\":{\"INFO\":{\"Codigo\":\"" + cod + "\",\"Descripcion\":\"" + descrip
				+ "\"},\"OUTPUT\":" + data + "}}";
		return dato;

	}

	public String classLogin(String description, String userID, String canalLogico, String canalFisico,
			String aplicacion, String date, String time) {
		String dato = "{\"type\":\"log\",\"description\":\"" + description + "\", \"rutUsuario\": \"" + userID
				+ "\", \"canalLogico\": \"" + canalLogico + "\", \"canalFisico\": \"" + canalFisico
				+ "\", \"aplication\": \"" + aplicacion + "\", \"date\": \"" + date + "\", \" hora_ingreso\": \"" + time
				+ "\"}";
		return dato;

	}

	public String login(String query) {

		JsonParser jsonParser = new JsonParser();
		JsonObject request = jsonParser.parse(query).getAsJsonObject();
		String respon;
		String rutUsuario = request.getAsJsonObject("Cabecera").get("RutUsuario").getAsString();
		String idUsuario = request.getAsJsonObject("Entrada").getAsJsonObject("query").getAsJsonObject("term")
				.get("_id").getAsString();

		JsonElement entrada = request.get("Entrada");

		if (rutUsuario.equals(idUsuario)) {
			String req;
			if (entrada == null) {
				req = "{}";
			} else {
				req = entrada.toString();
			}

			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);

			HttpEntity<String> request1 = new HttpEntity<String>(req.toString(), headers);
			String responseEntity = restTemplate.postForObject(endpoint + "/_search", request1, String.class);

			JsonObject requestLogin = jsonParser.parse(responseEntity).getAsJsonObject();
			String statusLogin = requestLogin.getAsJsonObject("hits").get("total").getAsString();

			if (statusLogin.equalsIgnoreCase("0")) {
				respon = response("Login_Response", "16", "No se encuentran elementos", "{}");
			} else
				respon = response("Login_Response", "00", "operacion exitosa", responseEntity);

			try {
				return respon;
			} catch (Exception e) {
				return response("Login_Response", "16", "No se encuentran elementos", "{}");
			}
		} else
			return response("Login_Response", "16", "No se encuentran elementoss", "{}");
	}

	public String LogInsert(String data) {

		JsonParser jsonParser = new JsonParser();
		JsonObject request = jsonParser.parse(data).getAsJsonObject();
		String desciption = request.getAsJsonObject("Entrada").get("desciption").getAsString();
		String rutUsuario = request.getAsJsonObject("Entrada").get("rutUsuario").getAsString();
		String canalLogico = request.getAsJsonObject("Entrada").get("canalLogico").getAsString();
		String canalFisico = request.getAsJsonObject("Entrada").get("canalFisico").getAsString();
		String aplicacion = request.getAsJsonObject("Entrada").get("aplicacion").getAsString();
		Format formatterTime = new SimpleDateFormat("HH:mm:ss");
		String timeTrans = formatterTime.format(new Date());
		Format formatterDate = new SimpleDateFormat("yyyy-MM-dd");
		String dateTrans = formatterDate.format(new Date());
		String dataElastic = classLogin(desciption, rutUsuario, canalLogico, canalFisico, aplicacion, dateTrans,
				timeTrans);

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> request1 = new HttpEntity<String>(dataElastic.toString(), headers);
		String responseEntity = restTemplate.postForObject(endpoint, request1, String.class);
		String respon = response("Logger_Response", "00", "operacion exitosa", responseEntity);
		try {
			return respon;
		} catch (Exception e) {
			return response("Logger_Response", "16", "No se a podido Insertar", "{}");
		}

	}

}
