package com.springBoot.restapi.model;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;


import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author elianaurrutia
 * public class App : metodos insetGeneric, search, update, ProdVersion contienen los repositorios de respuesta a ElasticsearchDB
 */
@Repository
public class App {

	private static int NO_UPDATE = 0;
	private LoginDao loginDao;


	private String port = "9200";

	private String INDEX = "handshakemanager";
	private String TYPE = "_doc";
	private String endpoint = "http://localhost:" + port + "/" + INDEX + "/" + TYPE;

	public App(LoginDao loginDao) {
		super();
		this.loginDao = loginDao;
	}

	public String response(String nameResponse, String cod, String descrip, String data) {
		String dato = "{\"" + nameResponse + "\":{\"INFO\":{\"Codigo\":\"" + cod + "\",\"Descripcion\":\"" + descrip
				+ "\"},\"OUTPUT\":" + data + "}}";
		return dato;

	}
	
	public String appProductiva(String rutUsuario, String CodigoAplicacion, String CodigoCanal, String data, String nameApp) {
		String dataApp = "{\"Cabecera\": {\"RutUsuario\": \""+rutUsuario+"\"},\"Entrada\": {\"type\": \"appproductiva\",\"nameApp\": \""+nameApp+"\",\"CodigoAplicacion\": \""+CodigoAplicacion+"\",\"CodigoCanal\": \""+CodigoCanal+"\",\"data\": ["+data+"],\"Ip\": \"ip\"}}";
		return dataApp;
	}

	public String structureLogin(String rut) {
		String login = "{\"Cabecera\":{\"RutUsuario\":\"" + rut + "\"},\"Entrada\":{\"query\":{\"term\":{\"_id\":\""
				+ rut + "\"}}}}";
		return login;
	}

	public String serchApp(String rut, String codApp) {
		String login = "{ \"Cabecera\": { \"RutUsuario\":\"" + rut
				+ "\" }, \"Entrada\": { \"query\": { \"bool\": { \"filter\": [ { \"term\": { \"type\": \"appproductiva\" } }, { \"term\": { \"CodigoAplicacion\":\""
				+ codApp + "\" } } ] } } } }";
		return login;
	}

	public String InsertByTipe(String data) {

		JsonParser jsonParser = new JsonParser();
		JsonObject request = jsonParser.parse(data).getAsJsonObject();
		JsonElement entrada = request.get("Entrada");
		String rutUsuario = request.getAsJsonObject("Cabecera").get("RutUsuario").getAsString();
		String type = request.getAsJsonObject("Entrada").get("type").getAsString();
		String queryLogin = structureLogin(rutUsuario);
		String responseLogin = loginDao.login(queryLogin);
		JsonObject dataParse = jsonParser.parse(responseLogin).getAsJsonObject();
		String info = dataParse.getAsJsonObject("Login_Response").get("INFO").toString();
		String codiParse = jsonParser.parse(info).getAsJsonObject().get("Codigo").toString();

		if (codiParse == "\"16\"") {
			return responseLogin;
		}

		if (entrada == null || entrada.toString().length() == 0) {
			return response("InsertData_Response", "16", "No se a podido Insertar", "{}");
		}
		
		if(type.equals("appproductiva")) { 
			String appProductiva = productiveVersion(data);
			request = jsonParser.parse(appProductiva).getAsJsonObject();
			entrada = request.get("Entrada");
		}

		String req;
		req = entrada.toString();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> request1 = new HttpEntity<String>(req.toString(), headers);
		String responseEntity = restTemplate.postForObject(endpoint, request1, String.class);
		String respon = response("InsertData_Response", "00", "operacion exitosa", responseEntity);
		try {
			return respon;
		} catch (Exception e) {
			return response("InsertData_Response", "16", "No se a podido Insertar", "{}");
		}

	}
	
public String productiveVersion(String app) {
		
		JsonParser jsonParser = new JsonParser();
		JsonObject request = jsonParser.parse(app).getAsJsonObject();
				
		String codAppSol = request.getAsJsonObject("Entrada").get("codApp").getAsString().toLowerCase();
		String rutUsuario = request.getAsJsonObject("Cabecera").get("RutUsuario").getAsString();
		
		String nameApp = request.getAsJsonObject("Entrada").get("nameApp").getAsString().toLowerCase();
		String codigoCanal = request.getAsJsonObject("Entrada").get("codApp").getAsString().toLowerCase();
		String version = request.getAsJsonObject("Entrada").get("version").getAsString().toLowerCase();
		String OS = request.getAsJsonObject("Entrada").get("os").getAsString().toLowerCase();
		String build = request.getAsJsonObject("Entrada").get("build").getAsString().toLowerCase();
		String mensaje = request.getAsJsonObject("Entrada").get("mensaje").getAsString().toLowerCase();
		String bloquear = request.getAsJsonObject("Entrada").get("bloqueo").getAsString().toLowerCase();
		String boton = request.getAsJsonObject("Entrada").get("button").getAsString().toLowerCase();
		String data = "{ \"Version\":\"" + version +"\", \"OS\":\""+ OS +"\", \"Build\":\""+ build + "\", \"Mensaje\":\""+ mensaje + "\", \"Bloquear\":\"" + bloquear + "\", \"Boton\":\"" + boton + "\" }";
		String newApp = appProductiva(rutUsuario, codAppSol, codigoCanal, data, nameApp);

		String querySearch = serchApp(rutUsuario, codAppSol);
		String queryApp = searchByType(querySearch);
		JsonObject requestSearch = jsonParser.parse(queryApp).getAsJsonObject();
		
		String existVersion = requestSearch.getAsJsonObject("SearchData_Response").getAsJsonObject("OUTPUT").getAsJsonObject("hits").get("total").toString();
			
		String versionProd = requestSearch.getAsJsonObject("SearchData_Response").getAsJsonObject("OUTPUT")
				.getAsJsonObject("hits").getAsJsonArray("hits").toString();
		
		if(existVersion.equals("0")) {		
			return newApp;
		} else {
			JsonArray jsonArr = new JsonParser().parse(versionProd).getAsJsonArray();
			JsonObject dt = jsonArr.get(0).getAsJsonObject();
			JsonObject source = dt.get("_source").getAsJsonObject();
			JsonArray jsonArrData = source.get("data").getAsJsonArray();
			String newVersion = "{\"Version\":\""+version+"\",\"OS\":\""+OS+"\",\"Build\":\""+build+"\"}";
			jsonArrData.add(newVersion);
			//String updateApp = UpdateByType(String data, String id)
		}		
		
		return "app";		
	}

	public String InsertSolicitud(String data) {

		JsonParser jsonParser = new JsonParser();
		JsonObject request = jsonParser.parse(data).getAsJsonObject();
		JsonElement entrada = request.get("Entrada");
		String rutUsuario = request.getAsJsonObject("Cabecera").get("RutUsuario").getAsString();
		String queryLogin = structureLogin(rutUsuario);
		String responseLogin = loginDao.login(queryLogin);
		JsonObject dataParse = jsonParser.parse(responseLogin).getAsJsonObject();
		String info = dataParse.getAsJsonObject("Login_Response").get("INFO").toString();
		String codiParse = jsonParser.parse(info).getAsJsonObject().get("Codigo").toString();

		if (codiParse == "\"16\"") {
			return responseLogin;
		}

		// Inicio Validación Versión y Build
		String codAppSol = request.getAsJsonObject("Entrada").get("codApp").getAsString().toLowerCase();
		String versionSol = request.getAsJsonObject("Entrada").get("version").getAsString();
		String buildSol = request.getAsJsonObject("Entrada").get("build").getAsString();

		String querySearch = serchApp(rutUsuario, codAppSol);
		String responseSearch = searchByType(querySearch);

		JsonObject requestSearch = jsonParser.parse(responseSearch).getAsJsonObject();

		String versionProd = requestSearch.getAsJsonObject("SearchData_Response").getAsJsonObject("OUTPUT")
				.getAsJsonObject("hits").getAsJsonArray("hits").toString();

		JsonArray jsonArr = new JsonParser().parse(versionProd).getAsJsonArray();
		JsonObject dt = jsonArr.get(0).getAsJsonObject();
		JsonObject aa = dt.get("_source").getAsJsonObject();
		JsonArray jsonArr2 = aa.get("data").getAsJsonArray();
		int largo = jsonArr2.size(), i = 0;
		String dat2 = String.valueOf(largo);
		int large = Integer.parseInt(dat2);

		for (; i < large; i++) {
			JsonObject dt2 = jsonArr2.get(i).getAsJsonObject();
			String versionGet = dt2.get("Version").getAsString();
			String buildGet = dt2.get("Build").getAsString();

			String separador = Pattern.quote(",");
			String[] parts = buildGet.split(separador);

			if (versionSol.equals(versionGet)) {
				for (int j = 0; j < parts.length; j++) {
					if (buildSol.equals(parts[j])) {
						return response("InsertSolicitud_Response", "16", "No se a podido Insertar", "{}");
					}
				}
			}

		}

		/*
		 * if(versionSol.equals(versionProd)) {
		 * 
		 * 
		 * }
		 */

		// Fin Validación Versión y Build

		if (entrada == null || entrada.toString().length() == 0) {
			return response("InsertSolicitud_Response", "16", "No se a podido Insertar", "{}");
		}

		String req;
		req = entrada.toString();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> request1 = new HttpEntity<String>(req.toString(), headers);
		String responseEntity = restTemplate.postForObject(endpoint, request1, String.class);
		String respon = response("InsertSolicitud_Response", "00", "operacion exitosa", responseEntity);
		try {
			return respon;
		} catch (Exception e) {
			return response("InsertSolicitud_Response", "16", "No se a podido Insertar", "{}");
		}

	}

	public String searchByType(String query) {

		JsonParser jsonParser = new JsonParser();
		JsonObject request = jsonParser.parse(query).getAsJsonObject();

		JsonElement entrada = request.get("Entrada");

		String rutUsuario = request.getAsJsonObject("Cabecera").get("RutUsuario").getAsString();
		String queryLogin = structureLogin(rutUsuario);
		String responseLogin = loginDao.login(queryLogin).toString();
		JsonObject dataParse = jsonParser.parse(responseLogin).getAsJsonObject();
		String info = dataParse.getAsJsonObject("Login_Response").get("INFO").toString();
		String codiParse = jsonParser.parse(info).getAsJsonObject().get("Codigo").toString();

		if (codiParse == "\"16\"") {
			return responseLogin;
		}

		String req;
		if (entrada == null) {
			req = "{}";
		} else {
			req = entrada.toString();
		}

		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> request1 = new HttpEntity<String>(req.toString(), headers);
		String responseEntity = restTemplate.postForObject(endpoint + "/_search", request1, String.class);

		String respon = response("SearchData_Response", "00", "operacion exitosa", responseEntity);

		try {
			return respon;
		} catch (Exception e) {
			return response("SearchData_Response", "16", "No se encuentran elementos", "{}");
		}

	}

	public String UpdateByType(String data, String id) {
		JsonParser jsonParser = new JsonParser();
		JsonObject request = jsonParser.parse(data).getAsJsonObject();
		JsonElement entrada = request.get("Entrada");

		String rutUsuario = request.getAsJsonObject("Cabecera").get("RutUsuario").getAsString();
		String queryLogin = structureLogin(rutUsuario);
		String responseLogin = loginDao.login(queryLogin);
		JsonObject dataParse = jsonParser.parse(responseLogin).getAsJsonObject();
		String info = dataParse.getAsJsonObject("Login_Response").get("INFO").toString();
		String codiParse = jsonParser.parse(info).getAsJsonObject().get("Codigo").toString();

		if (codiParse == "\"16\"") {
			return responseLogin;
		}

		String req = "{\"" + entrada + "}";
		req = entrada.toString();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> request1 = new HttpEntity<String>(req.toString(), headers);
		String responseEntity = restTemplate.postForObject(endpoint + "/" + id + "/_update", request1, String.class);

		JsonParser parser = new JsonParser();
		JsonObject resUpdate = parser.parse(responseEntity).getAsJsonObject();
		JsonElement noop = resUpdate.get("result");
		String cabezeraNoop = noop.toString().replace("\"", "");
		int dato = cabezeraNoop.trim().compareTo("noop");

		try {
			if (dato == NO_UPDATE) {
				String responNoop = response("UpdateData_Response", "04", "No hay elementos que actualizar",
						responseEntity);
				return responNoop;
			} else {
				String respon = response("UpdateData_Response", "00", "operacion exitosa", responseEntity);
				return respon;
			}

		} catch (Exception e) {
			return response("UpdateData_Response", "16", "No se a podido Actualizar", "{}");
		}

	}

	public String responseProductiva(String version, String build, String OS, String bloquear, String mensaje,
			String fecha, String boton) {
		if (mensaje == null || bloquear == null) {
			return "{\"aplicacion\":{\"Version\":\"" + version + "\", \"Build\":\"" + build + "\", \"OS\": \"" + OS
					+ "\",\"Bloquear\":null,\"Mensaje\":null,\"Url\":null,\"Boton\":null,\"Fecha\":\"" + fecha + "\"}}";
		} else {
			return "{\"aplicacion\":{\"Version\":\"" + version + "\", \"Build\":\"" + build + "\", \"OS\": \"" + OS
					+ "\",\"Bloquear\":\"" + bloquear + "\",\"Mensaje\":\"" + mensaje + "\",\"Url\":null,\"Boton\":\""
					+ boton + "\",\"Fecha\":\"" + fecha + "\"}}";
		}
	}

	public String searchVersion(String vesion, String codApp) {
		return "{\"query\":{\"bool\":{\"must\":[{\"term\":{\"CodigoAplicacion\":\"" + codApp
				+ "\"}},{\"term\":{\"data.Version\":\"" + vesion + "\"}},{\"term\":{\"type\":\"appproductiva\"}}]}}}";
	}

	public String queryVersionProductive(String data) {
		String respon = null;
		Format formatterTime = new SimpleDateFormat("HH:mm:ss");
		String timeTrans = formatterTime.format(new Date());
		Format formatterDate = new SimpleDateFormat("dd-MM-yyyy");
		String dateTrans = formatterDate.format(new Date());

		String fecha = dateTrans + " " + timeTrans;

		JsonParser jsonParser = new JsonParser();
		JsonObject request = jsonParser.parse(data).getAsJsonObject();

		String CodigoAplicacion = request.getAsJsonObject("Entrada").get("CodigoAplicacion").getAsString()
				.toLowerCase();
		String CodigoCanal = request.getAsJsonObject("Entrada").get("CodigoCanal").getAsString();
		String OSNormal = request.getAsJsonObject("Entrada").get("OS").getAsString();
		String OS = request.getAsJsonObject("Entrada").get("OS").getAsString().toLowerCase();
		String Version = request.getAsJsonObject("Entrada").get("Version").getAsString();
		String build = request.getAsJsonObject("Entrada").get("Build").getAsString();

		// Estructura de query elasticsearch
		String req = searchVersion(Version, CodigoAplicacion);

		// llamada a api elasticsearch
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> request1 = new HttpEntity<String>(req.toString(), headers);
		String responseEntity = restTemplate.postForObject(endpoint + "/_search", request1, String.class);

		// se recorre la respuesta
		JsonObject responseEntityT = jsonParser.parse(responseEntity).getAsJsonObject();
		String resultSearch = responseEntityT.getAsJsonObject("hits").get("total").toString();

		int dat = Integer.parseInt(resultSearch);
		// try {
		if (dat == 0) {
			respon = response("VersionPruductive_Response", "00", "operacion exitosa",
					responseProductiva(Version, build, OS.toUpperCase(), null, null, fecha, null));

		} else {
			String resultSearch2 = responseEntityT.getAsJsonObject("hits").getAsJsonArray("hits").toString();
			JsonArray jsonArr = new JsonParser().parse(resultSearch2).getAsJsonArray();
			JsonObject dt = jsonArr.get(0).getAsJsonObject();
			JsonObject aa = dt.get("_source").getAsJsonObject();
			JsonArray jsonArr2 = aa.get("data").getAsJsonArray();
			int largo = jsonArr2.size(), i = 0;
			String dat2 = String.valueOf(largo);
			int large = Integer.parseInt(dat2);

			for (; i < large; i++) {

				JsonObject dt2 = jsonArr2.get(i).getAsJsonObject();
				String versionGet = dt2.get("Version").getAsString();
				String osGet = dt2.get("OS").getAsString();
				String buildGet = dt2.get("Build").getAsString();
				String bloquearResult = dt2.get("Bloquear").getAsString();
				String mensajeResult = dt2.get("Mensaje").getAsString();
				String botonResult = dt2.get("Boton").getAsString();
				if (versionGet.equals(Version) && buildGet.equals(build) == false || osGet.equals(OSNormal) == false) {
					respon = response("VersionPruductive_Response", "00", "operacion exitosa",
							responseProductiva(Version, build, OSNormal, null, null, fecha, null));
				}
				if (versionGet.equals(Version) && buildGet.equals(build) && osGet.equals(OSNormal)) {
					respon = response("VersionPruductive_Response", "00", "operacion exitosa", responseProductiva(
							Version, build, OSNormal, bloquearResult, mensajeResult, fecha, botonResult));
				}

			}

		}
		try {

			return respon;
		} catch (Exception e) {
			return response("VersionPruductive_Response", "16", "No se encuentran elementos", "{}");
		}

	}

}
