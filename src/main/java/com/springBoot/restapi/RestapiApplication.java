package com.springBoot.restapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

@SpringBootApplication
public class RestapiApplication extends SpringBootServletInitializer {
	
	
	public RestapiApplication() {
	    super();
	    setRegisterErrorPageFilter(false); // <- this one
	}
	
	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(RestapiApplication.class);
    }

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(RestapiApplication.class, args);
		  DispatcherServlet dispatcherServlet = (DispatcherServlet)ctx.getBean("dispatcherServlet");
	      dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);
	}

}
