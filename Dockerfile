FROM pgstdvbap01.cl.bsch:8480/runtime/chile-runtime-jdk:jdk11

COPY ./build/libs/BackEnd-0.0.1.jar /opt/

ENTRYPOINT java -jar /opt/BackEnd-0.0.1.jar

EXPOSE 8080:9999
